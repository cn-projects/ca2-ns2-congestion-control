set ns [new Simulator]

$ns color 1 Blue
$ns color 2 Red

#Open the nam trace file
set nf [open outReno.nam w]
$ns namtrace-all $nf

set tracefile1 [open traceReno.tr w]
$ns trace-all $tracefile1


proc finish {} {
    global ns nf tracefile1
    $ns flush-trace
    #Close the trace file
    close $nf
    close $tracefile1
    #Executenam on the trace file
    exec nam outReno.nam &
    exit 0
}

proc generateVariableDelay {} {
    global randNum
    set randNum [expr int(20 * rand() + 5)]
    return $randNum
}

set n1 [$ns node]
set n2 [$ns node]
set n3 [$ns node]
set n4 [$ns node]
set n5 [$ns node]
set n6 [$ns node]

$n3 shape box
$n4 shape box

$ns duplex-link $n1 $n3 100Mb 5ms DropTail
$ns duplex-link $n5 $n4 100Mb 5ms DropTail
$ns duplex-link $n3 $n4 100Kb 1ms DropTail
$ns duplex-link $n5 $n4 100Mb 5ms DropTail

set variableDelay [generateVariableDelay]
$ns duplex-link $n2 $n3 100Mb [expr $variableDelay]ms DropTail
$ns duplex-link $n6 $n4 100Mb [expr $variableDelay]ms DropTail

$ns duplex-link-op $n3 $n1 orient left-up
$ns duplex-link-op $n3 $n2 orient left-down
$ns duplex-link-op $n4 $n5 orient right-up
$ns duplex-link-op $n3 $n4 orient right
$ns duplex-link-op $n4 $n6 orient right-down

$ns queue-limit $n1 $n3 10
$ns queue-limit $n2 $n3 10
$ns queue-limit $n4 $n5 10
$ns queue-limit $n4 $n6 10
$ns queue-limit $n3 $n4 10
#$ns queue-limit $n4 $n3 10


set tcp1 [new Agent/TCP/Newreno]
$ns attach-agent $n1 $tcp1

set tcp2 [new Agent/TCP/Newreno]
$ns attach-agent $n2 $tcp2

set sink1 [new Agent/TCPSink]
$ns attach-agent $n5 $sink1

set sink2 [new Agent/TCPSink]
$ns attach-agent $n6 $sink2

$ns connect $tcp1 $sink1
$ns connect $tcp2 $sink2

$tcp1 set fid_ 1
$tcp1 set packetSize_ 1000
$tcp1 set ttl_ 64
$tcp1 set maxxcwnd_ 100

$tcp2 set fid_ 2
$tcp2 set packetSize_ 1000
$tcp2 set ttl_ 64
$tcp2 set maxcwnd_ 100

set ftp1 [new Application/FTP]
$ftp1 attach-agent $tcp1

set ftp2 [new Application/FTP]
$ftp2 attach-agent $tcp2

$tcp1 attach $tracefile1
$tcp1 tracevar cwnd_
$tcp1 tracevar rtt_

$tcp2 attach $tracefile1
$tcp2 tracevar cwnd_
$tcp2 tracevar rtt_

$ns at 1 "$ftp1 start"
$ns at 1 "$ftp2 start"

$ns at 1000 "$ftp1 stop"
$ns at 1000 "$ftp2 stop"

$ns at 1005 "finish"

#Run the simulation
$ns run


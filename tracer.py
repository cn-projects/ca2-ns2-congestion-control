import os
import matplotlib.pyplot as plt

scriptNames = ['tcp_newReno.tcl', 'tcp_tahoe.tcl', 'tcp_vegas.tcl']
tracerFileNames = ['traceReno.tr', 'traceTahoe.tr', 'traceVegas.tr']
tags = ['+', '-', 'r', 'd', 's']


def runTCLScript(fileName, tracerFileName, count):
    traceResults = []
    for i in range(count):
        os.system("ns " + fileName)
        tracerFile = open(tracerFileName, 'r')
        lines = tracerFile.readlines()
        traceResults.append(lines)
    return traceResults

def findCWNDChanges(traceResults):
    cwnd1 = []
    cwnd2 = []
    lastCWND1 = 0
    lastCWND2 = 0
    lastTime = 2
    for line in traceResults:
        if line[5] == 'cwnd_' and int(float(line[0])) == lastTime:
            if line[1] == '0':
                lastCWND1 = float(line[-1])
            else:
                lastCWND2 = float(line[-1])
        elif line[0] not in tags and int(float(line[0])) > lastTime:
            lastTime += 1
            cwnd1.append(lastCWND1)
            cwnd2.append(lastCWND2)
            if line[5] == 'cwnd_' and line[1] == '0':
                lastCWND1 = float(line[-1])
            elif line[5] == 'cwnd_' and line[1] == '1':
                lastCWND2 = float(line[-1])
    return [cwnd1, cwnd2]

def findGoodPut(traceResults):
    goodPut1 = []
    goodPut2 = []
    count1 = 0
    count2 = 0
    lastTime = 2
    for line in traceResults:
        if line[0] == 'r' and int(float(line[1])) == lastTime and line[4] == 'ack':
            if line[7] == '1' and int(line[3]) == 0:
                count1 += 1
            elif line[7] == '2' and int(line[3]) == 1:
                count2 += 1
        elif int(float(line[1])) > lastTime:
            lastTime += 1
            goodPut1.append(count1)
            goodPut2.append(count2)
            count1 = 0
            count2 = 0
            if line[0] == 'r' and line[4] == 'ack' and line[7] == '1' and int(line[3]) == 0:
                count1 = 1
            elif line[0] == 'r' and line[4] == 'ack' and line[7] == '2' and int(line[3]) == 1:
                count2 = 1
    return [goodPut1, goodPut2]

def findDropRate(traceResults):
    dropCount1 = []
    dropCount2 = []
    count1 = 0
    count2 = 0
    lastTime = 2
    for line in traceResults:
        if line[0] == 'd' and int(float(line[1])) == lastTime:
            if line[7] == '1':
                count1 += 1
            else:
                count2 += 1
        elif line[1] not in tags and int(float(line[1])) > lastTime:
            lastTime += 1
            dropCount1.append(count1)
            dropCount2.append(count2)
            count1 = 0
            count2 = 0
            if line[0] == 'd' and line[7] == '1':
                count1 = 1
            elif line[0] == 'd' and line[7] == '2':
                count2 = 1
    return dropCount1, dropCount2

def findRTT(traceResults):
    rtt1 = []
    rtt2 = []
    lastRTT1 = 0
    lastRTT2 = 0
    lastTime = 2
    for line in traceResults:
        if line[5] == 'rtt_' and int(float(line[0])) == lastTime:
            if line[1] == '0':
                lastRTT1 = float(line[-1])
            else:
                lastRTT2 = float(line[-1])
        elif line[0] not in tags and int(float(line[0])) > lastTime:
            lastTime += 1
            rtt1.append(lastRTT1)
            rtt2.append(lastRTT2)
            if line[5] == 'rtt_' and line[1] == '0':
                lastRTT1 = float(line[-1])
            elif line[5] == 'rtt' and line[1] == '1':
                lastRTT2 = float(line[-1])
    return [rtt1, rtt2]

def findAverage(tcpTraceResult, count, divisor):
    finalResult1 = []
    finalResult2 = []
    for i in range(len(tcpTraceResult[0][0]) // divisor):
        sum1 = 0
        sum2 = 0
        for j in range(count):
            sum1 += tcpTraceResult[j][0][i]
            sum2 += tcpTraceResult[j][1][i]
        finalResult1.append(sum1/count)
        finalResult2.append(sum2/count)
    return [finalResult1, finalResult2]

def plot(x, y, z, title, figureNum):
    plt.figure(figureNum)
    xAxis1 = [i + 2 for i in range(len(x[0]))]
    xAxis2 = [i + 2 for i in range(len(y[0]))]
    xAxis3 = [i + 2 for i in range(len(z[0]))]
    plt.plot(xAxis1, x[0], color = 'red', label = 'newReno tcp1')
    plt.plot(xAxis1, x[1], color = 'indianred', label = 'newReno tcp2')
    plt.plot(xAxis2, y[0], color = 'mediumturquoise', label = 'tahoe tcp1')
    plt.plot(xAxis2, y[1], color = 'blue', label = 'tahoe tcp2')
    plt.plot(xAxis3, z[0], color = 'green', label = 'vegas tcp1')
    plt.plot(xAxis3, z[1], color = 'lime', label = 'vegas tcp2')
    plt.title(title)
    plt.legend(bbox_to_anchor = (1.05, 1), loc = 'upper left')
    plt.tight_layout()

def calculateGoodPut(count, newRenoTraceResults, tahoeTraceResults, vegasTraceResults):
    newRenoGoodPut = []
    tahoeGoodPut = []
    vegasGoodPut = []
    for i in range(count):
        newRenoSplittedResult = [line.split() for line in newRenoTraceResults[i]]
        tahoeSplittedResult = [line.split() for line in tahoeTraceResults[i]]
        vegasSplittedResult = [line.split() for line in vegasTraceResults[i]]
        newRenoGoodPut.append(findGoodPut(newRenoSplittedResult))
        tahoeGoodPut.append(findGoodPut(tahoeSplittedResult))
        vegasGoodPut.append(findGoodPut(vegasSplittedResult))
    newRenoAverageGoodPut = findAverage(newRenoGoodPut, count, divisor)
    tahoeAverageGoodPut = findAverage(tahoeGoodPut, count, divisor)
    vegasAverageGoodPut = findAverage(vegasGoodPut, count, divisor)
    plot(newRenoAverageGoodPut, tahoeAverageGoodPut, vegasAverageGoodPut, "Goodput Rate", 1)

def calculateCWND(count, newRenoTraceResults, tahoeTraceResults, vegasTraceResults):
    newRenoCWND = []
    tahoeCWND = []
    vegasCWND = []
    for i in range(count):
        newRenoSplittedResult = [line.split() for line in newRenoTraceResults[i]]
        tahoeSplittedResult = [line.split() for line in tahoeTraceResults[i]]
        vegasSplittedResult = [line.split() for line in vegasTraceResults[i]]
        newRenoCWND.append(findCWNDChanges(newRenoSplittedResult))
        tahoeCWND.append(findCWNDChanges(tahoeSplittedResult))
        vegasCWND.append(findCWNDChanges(vegasSplittedResult))
    newRenoAverageCWND = findAverage(newRenoCWND, count, divisor)
    tahoeAverageCWND = findAverage(tahoeCWND, count, divisor)
    vegasAverageCWND = findAverage(vegasCWND, count, 1)
    plot(newRenoAverageCWND, tahoeAverageCWND, vegasAverageCWND, "CWND Changes", 2)

def calculateRTT(count, newRenoTraceResults, tahoeTraceResults, vegasTraceResults):
    newRenoRTT = []
    tahoeRTT = []
    vegasRTT = []
    for i in range(count):
        newRenoSplittedResult = [line.split() for line in newRenoTraceResults[i]]
        tahoeSplittedResult = [line.split() for line in tahoeTraceResults[i]]
        vegasSplittedResult = [line.split() for line in vegasTraceResults[i]]
        newRenoRTT.append(findRTT(newRenoSplittedResult))
        tahoeRTT.append(findRTT(tahoeSplittedResult))
        vegasRTT.append(findRTT(vegasSplittedResult))
    newRenoAverageRTT = findAverage(newRenoRTT, count, divisor)
    tahoeAverageRTT = findAverage(tahoeRTT, count, divisor)
    vegasAverageRTT = findAverage(vegasRTT, count, 1)
    plot(newRenoAverageRTT, tahoeAverageRTT, vegasAverageRTT, "RTT", 3)

def calculateDropRate(count, newRenoTraceResults, tahoeTraceResults, vegasTraceResults):
    newRenoDropRate = []
    tahoeDropRate = []
    vegasDropRate = []
    for i in range(count):
        newRenoSplittedResult = [line.split() for line in newRenoTraceResults[i]]
        tahoeSplittedResult = [line.split() for line in tahoeTraceResults[i]]
        vegasSplittedResult = [line.split() for line in vegasTraceResults[i]]
        newRenoDropRate.append(findDropRate(newRenoSplittedResult))
        tahoeDropRate.append(findDropRate(tahoeSplittedResult))
        vegasDropRate.append(findDropRate(vegasSplittedResult))
    newRenoAverageDropRate = findAverage(newRenoDropRate, count, divisor)
    tahoeAverageDropRate = findAverage(tahoeDropRate, count, divisor)
    vegasAverageDropRate = findAverage(vegasDropRate, count, divisor)
    plot(newRenoAverageDropRate, tahoeAverageDropRate, vegasAverageDropRate, "Drop Rate", 4)

count = 10
divisor = 16
newRenoTraceResults = runTCLScript(scriptNames[0], tracerFileNames[0], count)
tahoeTraceResults = runTCLScript(scriptNames[1], tracerFileNames[1], count)
vegasTraceResults = runTCLScript(scriptNames[2], tracerFileNames[2], count)
calculateGoodPut(count, newRenoTraceResults, tahoeTraceResults, vegasTraceResults)
calculateCWND(count, newRenoTraceResults, tahoeTraceResults, vegasTraceResults)
calculateRTT(count, newRenoTraceResults, tahoeTraceResults, vegasTraceResults)
calculateDropRate(count, newRenoTraceResults, tahoeTraceResults, vegasTraceResults)
plt.show()


